#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
\input{../config/preamble.tex}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize b5paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 1
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter*
Acknowledgements
\end_layout

\begin_layout Standard
Doing a PhD was a lot of fun.
 Messing around with electronics and simulators is a great way to spend
 four years, especially when it all happens at the VUB.
\end_layout

\begin_layout Standard
Research is teamwork and I was part of a great team.
 Without the expertise of the following people, this thesis would not have
 been.
 I would like to thank Gerd, Rik and Yves for always being there to answer
 questions.
 Thanks to Ebrahim, Francesco, Maarten, Egon, Piet and Dries for helping
 me with the dirty details of the science, the programming and the writing.
 All this cannot be done without the excellent technical support provided
 by Sven, Ann and Johan.
\end_layout

\begin_layout Standard
For the part on stability analysis I would like to thank Fabien, Martine,
 Laurent and Sylvain for the invitation to France and for the fruitful collabora
tion.
\end_layout

\begin_layout Standard
Thanks to the members of my jury for evaluating my work and providing constructi
ve feedback to improve my thesis.
 And I would also like to thank Lennert Gavel for helping me with the lay-out
 of this thesis.
\end_layout

\begin_layout Standard
I would like to thank my friends and colleagues.
 Hannes, Maral, Matthias, Evy, Freddie, Nils, Wesley, Maarten and Glenn
 for great times in Brussels.
 Io and the gang for great times in Aalst.
 Ariane en Nick for great times in Ghent.
 Karsten and Joerie for letting me win in squash every time.
 The people of the PhD network for the nice events we were able to organise.
 And my colleagues Alexander, Anna, Evy, Georgios, Gabriel, Jean, Jan, Johan,
 John, Jos, Leo, Mark and Philippe for making ELEC great (again).
\end_layout

\begin_layout Standard
Finally I would like to thank my family.
 Mama, Papa, Meter, Moeke en Vake for dealing with my surprise visits.
 Tante V, tante Sophie, Io, Seb en Vita for being awesome.
\end_layout

\end_body
\end_document
