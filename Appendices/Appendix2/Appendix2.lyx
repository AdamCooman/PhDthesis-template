#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
\input{../config/preamble.tex}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize b5paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 1
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
glsresetall
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Testing the Large-Signal Small-Signal simulator in ADS
\begin_inset CommandInset label
LatexCommand label
name "chap:LSSStest"

\end_inset


\end_layout

\begin_layout Quote

\emph on
Chapter
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Stability-Analysis"

\end_inset

 uses the 
\emph off

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{LSSS}
\end_layout

\end_inset


\emph on
 simulator to determine the linearisation around a periodic orbit of a circuit.
 In this appendix, we verify the accuracy of the 
\emph off

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{LSSS}
\end_layout

\end_inset


\emph on
 simulator in 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{ADS}
\end_layout

\end_inset

.
 To do this, a test system with known frequency responses is simulated and
 the simulation results are compared to the exact expressions to discover
 the error level in the simulations.
 
\end_layout

\begin_layout Quote

\emph on
We discover that the error made in the simulation is rather large when the
 default settings are used.
 By configuring the simulator correctly, a better result can be obtained.
 This work has been done with the help of Ebrahim Louarroudi.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
glsresetall
\end_layout

\end_inset

 A 
\emph off

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{LSSS}
\end_layout

\end_inset


\emph default
 simulation calculates the linearised response around a periodic solution
 of a circuit to a small-signal excitation.
 
\end_layout

\end_body
\end_document
